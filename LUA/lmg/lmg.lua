_initM249Tweak = _initM249Tweak or WeaponFactoryTweakData._init_m249
function WeaponFactoryTweakData:_init_m249()
	_initM249Tweak(self)
	for _,part in ipairs(self.parts.wpn_fps_shot_r870_s_folding.forbids) do
		if ( part ~= "wpn_fps_upg_o_acog" and part ~= "wpn_fps_shot_r870_ris_special" ) then
			table.insert(self.wpn_fps_lmg_m249.uses_parts, part)
		end
	end
end

_initRPKTweak = _initRPKTweak or WeaponFactoryTweakData._init_rpk
function WeaponFactoryTweakData:_init_rpk()
	_initRPKTweak(self)
	for _,part in ipairs(self.parts.wpn_fps_shot_r870_s_folding.forbids) do
		if ( part ~= "wpn_fps_upg_o_acog" and part ~= "wpn_fps_shot_r870_ris_special" ) then
			table.insert(self.wpn_fps_lmg_rpk.uses_parts, part)
		end
	end
end

_initBrennerTweak = _initBrennerTweak or WeaponFactoryTweakData._init_hk21
function WeaponFactoryTweakData:_init_hk21()
	_initBrennerTweak(self)
	for _,part in ipairs(self.parts.wpn_fps_shot_r870_s_folding.forbids) do
		if ( part ~= "wpn_fps_upg_o_acog" and part ~= "wpn_fps_shot_r870_ris_special" ) then
			table.insert(self.wpn_fps_lmg_hk21.uses_parts, part)
		end
	end
end