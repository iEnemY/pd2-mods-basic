_initM249 = _initM249 or PlayerTweakData._init_m249
function PlayerTweakData:_init_m249()
	_initM249(self)
	local pivot_shoulder_translation = Vector3( 10.775, 5.09, -1.2 )
	local pivot_shoulder_rotation = Rotation( 0, -58.75, 0 )
	local pivot_head_rotation = Rotation( 0, 0.2, 0 )
	self.stances.m249.steelsight.shoulders.translation = Vector3( 0, 5.5, .75 ) - pivot_shoulder_translation:rotate_with( pivot_shoulder_rotation:inverse() ):rotate_with( pivot_head_rotation )
	self.stances.m249.steelsight.shoulders.rotation = pivot_head_rotation
end

_initRPK = _initRPK or PlayerTweakData._init_rpk
function PlayerTweakData:_init_rpk()
	_initRPK(self)
	local pivot_shoulder_translation = Vector3( 10.69, 33, -1.84 )
	local pivot_shoulder_rotation = Rotation( 0.1067, -0.0850111, 0.629008 )
	local pivot_head_rotation = Rotation( 0, 0.2, 0 )
	self.stances.rpk.steelsight.shoulders.translation = Vector3( .1, 7, 0.22 ) - pivot_shoulder_translation:rotate_with( pivot_shoulder_rotation:inverse() ):rotate_with( pivot_head_rotation )
	self.stances.rpk.steelsight.shoulders.rotation = pivot_head_rotation
end

_initBrenner = _initBrenner or PlayerTweakData._init_hk21
function PlayerTweakData:_init_hk21()
	_initBrenner(self)
	local pivot_shoulder_translation = Vector3( 10.83, 26, 1.37 )
	local pivot_shoulder_rotation = Rotation( 3.03061, 1.08595, 1.87441 )
	local pivot_head_rotation = Rotation( -3, -1, -2 )
	self.stances.hk21.steelsight.shoulders.translation = Vector3( .98, 10, 0.1 ) - pivot_shoulder_translation:rotate_with( pivot_shoulder_rotation:inverse() ):rotate_with( pivot_head_rotation )
	self.stances.hk21.steelsight.shoulders.rotation = pivot_head_rotation
end