if not InfBodyBags then
    if inGame() and isPlaying() and not inChat() then
			--Unlimited bodybags
		old_interact_blocked = old_interact_blocked or IntimitateInteractionExt._interact_blocked
		function IntimitateInteractionExt:_interact_blocked( ... )
			if self.tweak_data == "hostage_convert" then
				return not (managers.player:has_category_upgrade( "player", "convert_enemies" ) and not managers.player:chk_minion_limit_reached() )
			elseif self.tweak_data == "corpse_dispose" and not managers.player.carry_stack then
				if not managers.player:has_category_upgrade( "player", "corpse_dispose" ) or managers.player:is_carrying() then
					return true
				end
				return not managers.player:can_carry( "person" )
			else
				return old_interact_blocked(self, ...)
			end
		end

		managers.interaction._interact_blocked = IntimitateInteractionExt._interact_blocked
        Print("Infinite bodybags are live")
		InfBodyBags = true  
	end
end