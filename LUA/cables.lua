if not Cables30 then
    Cables30 = true  
	function PlayerManager:add_special(params)
		local name = params.equipment or params.name
		if not tweak_data.equipments.specials[name] then
			Application:error("Special equipment " .. name .. " doesn't exist!")
			return
		end
		local unit = self:player_unit()
		local respawn = params.amount and true or false
		local equipment = tweak_data.equipments.specials[name]
		local special_equipment = self._equipment.specials[name]
		local amount = params.amount or equipment.quantity
		local extra = self:_equipped_upgrade_value(equipment) + self:upgrade_value(name, "quantity")
		if name == "cable_tie" then
			extra = 30 -- self:upgrade_value(name, "quantity_1") + self:upgrade_value(name, "quantity_2")
		end
		if special_equipment then
			if equipment.quantity then
				local dedigested_amount = Application:digest_value(special_equipment.amount, false)
				local new_amount = self:has_category_upgrade(name, "quantity_unlimited") and -1 or math.min(dedigested_amount + amount, equipment.quantity + extra)
				special_equipment.amount = Application:digest_value(new_amount, true)
				if special_equipment.is_cable_tie then
					managers.hud:set_cable_ties_amount(HUDManager.PLAYER_PANEL, new_amount)
					self:update_synced_cable_ties_to_peers(new_amount)
				else
					managers.hud:set_special_equipment_amount(name, new_amount)
					self:update_equipment_possession_to_peers(name, new_amount)
				end
			end
			return
		end
		local icon = equipment.icon
		local action_message = equipment.action_message
		local dialog = equipment.dialog_id
		if not params.silent then
			local text = managers.localization:text(equipment.text_id)
			local title = managers.localization:text("present_obtained_mission_equipment_title")
			managers.hud:present_mid_text({
				text = text,
				title = title,
				icon = icon,
				time = 4
			})
			if dialog then
				managers.dialog:queue_dialog(dialog, {})
			end
			if action_message and alive(unit) then
				managers.network:session():send_to_peers_synched("sync_show_action_message", unit, action_message)
			end
		end
		local quantity = (not self:has_category_upgrade(name, "quantity_unlimited") or not -1) and equipment.quantity and (not respawn or not math.min(params.amount, equipment.quantity + extra)) and equipment.quantity and math.min(amount + extra, equipment.quantity + extra)
		local is_cable_tie = name == "cable_tie"
		if is_cable_tie then
			managers.hud:set_cable_tie(HUDManager.PLAYER_PANEL, {
				icon = icon,
				amount = quantity or nil
			})
			self:update_synced_cable_ties_to_peers(quantity)
		else
			managers.hud:add_special_equipment({
				id = name,
				icon = icon,
				amount = quantity or nil
			})
			self:update_equipment_possession_to_peers(name, quantity)
		end
		self._equipment.specials[name] = {
			amount = quantity and Application:digest_value(quantity, true) or nil,
			is_cable_tie = is_cable_tie
		}
		if equipment.player_rule then
			self:set_player_rule(equipment.player_rule, true)
		end
	end
end  